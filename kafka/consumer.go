package kafka

import (
	"context"
	// "fmt"

	"github.com/double/post_service/config"
	"github.com/double/post_service/kafka/handler"
	"github.com/double/post_service/pkg/logger"
	"github.com/double/post_service/pkg/messagebroker"
	"github.com/double/post_service/storage"
	"github.com/jmoiron/sqlx"
	"github.com/segmentio/kafka-go"
)

type KafkaConsumer struct {
	log logger.Logger
	KafkaHandler *handler.KafkaHandler
	KafkaConsumer *kafka.Reader
}

func(k KafkaConsumer) Start() {
	// fmt.Println("Consumer started..!")
	for {
		m, err := k.KafkaConsumer.ReadMessage(context.Background())
		// fmt.Println("Here it is: ",m, err )
		if err != nil {
			k.log.Error("Error while consuming the message", logger.Error(err))
			break
		}

		err = k.KafkaHandler.Handle(m.Value)
		if err != nil {
			k.log.Error("Failed to handle the consumer topic: ", logger.String("on topic", m.Topic))
		} else {
			k.log.Info("Successfuly consumer message",
			logger.String("on topic", m.Topic),
			logger.String("message", "success"))
		}
	}

	err := k.KafkaConsumer.Close()
	if err != nil {
		k.log.Error("Failed to close consumer", logger.Error(err))
	}
}

func NewKafkaConsumer(db *sqlx.DB, conf *config.Config, log logger.Logger, topic string) messagebroker.Consumer {
	connString := "localhost:8081"
	return &KafkaConsumer{
		KafkaConsumer: kafka.NewReader(
			kafka.ReaderConfig{
				Brokers: []string{connString},
			Topic: topic,
			MinBytes: 10e3,// 10 KB
			MaxBytes: 10e7,// 10 MB
			
		}),
		KafkaHandler: handler.NewKafkaHandler(*conf, log, storage.NewStoragePg(db)),
		log: log,
	}
}