package handler

import (
	"context"

	"github.com/double/post_service/config"
	p "github.com/double/post_service/genproto/post"
	"github.com/double/post_service/pkg/logger"
	"github.com/double/post_service/storage"
)

type KafkaHandler struct {
	log logger.Logger
	cfg config.Config
	storage storage.IStorage
}

func NewKafkaHandler(cfg config.Config,log logger.Logger, storage storage.IStorage) *KafkaHandler {
	return &KafkaHandler{
		log: log,
		cfg: cfg,
		storage: storage,
	}
}

func(h *KafkaHandler) Handle(value []byte) error {
	post := p.PostRequest{}
	err := post.Unmarshal(value)
	if err != nil {
		return err
	}

	_, err = h.storage.Post().CreatePost(context.Background(),&post)
	if err != nil {
		return err
	}
	return nil
}