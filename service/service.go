package service

import (
	"context"
	"log"

	p "github.com/double/post_service/genproto/post"
	u "github.com/double/post_service/genproto/user"
	"github.com/double/post_service/pkg/logger"
	grpcclient "github.com/double/post_service/service/grpc_client"
	"github.com/double/post_service/storage"
	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type PostService struct {
	storage storage.IStorage
	Logger  logger.Logger
	Client  grpcclient.Clients
}

func NewPostService(db *sqlx.DB, log logger.Logger, client grpcclient.Clients) *PostService {
	return &PostService{
		storage: storage.NewStoragePg(db),
		Logger:  log,
		Client:  client,
	}
}

func (s *PostService) CreatePost(ctx context.Context, req *p.PostRequest) (*p.PostResponse, error) {
	res, err := s.storage.Post().CreatePost(ctx, req)
	if err != nil {
		s.Logger.Error("error insert post", logger.Any("Error insert post", err))
		return &p.PostResponse{}, status.Error(codes.Internal, "something went wrong, please check post info")
	}

	user, err := s.Client.User().UserForCreate(ctx,&u.UserId{Id: req.OwnerId})
	if err != nil {
		log.Println("failed to getting user for post by id: ", err)
		return &p.PostResponse{}, err
	}
	res.Fullname = user.FirstName + " " + user.LastName

	return res, nil
}

func (s *PostService) GetPostById(ctx context.Context, req *p.PostId) (*p.PostResponse, error) {
	res, err := s.storage.Post().GetPostById(req.Id)
	if err != nil {
		s.Logger.Error("error get user", logger.Any("Error get user", err))
		return &p.PostResponse{}, status.Error(codes.Internal, "something went wrong, please check user info")
	}

	user, err := s.Client.User().UserForCreate(ctx, &u.UserId{Id: res.OwnerId})
	if err != nil {
		log.Println("failed to getting user for post by id: ", err)
		return &p.PostResponse{}, err
	}

	res.Fullname = user.FirstName + " " + user.LastName
	return res, nil
}

func (s *PostService) GetPostByUserId(ctx context.Context, req *p.UserId) (*p.Posts, error) {
	res, err := s.storage.Post().GetPostByUserId(req.Id)
	if err != nil {
		s.Logger.Error("error get post by user_id", logger.Any("Error get postby user_id", err))
		return &p.Posts{}, status.Error(codes.Internal, "something went wrong, please check post info")
	}
	return res, nil
}

func (s *PostService) UpdatePost(ctx context.Context, req *p.UpdatePostRequest) (*p.PostResponse, error) {
	res,err := s.storage.Post().UpdatePost(ctx, req)
	if err != nil {
		s.Logger.Error("error update post by user_id", logger.Any("Error update post by user_id", err))
		return &p.PostResponse{}, status.Error(codes.Internal, "something went wrong, please check post info")
	}
	_, err = s.Client.User().UserForCreate(ctx, &u.UserId{Id: res.OwnerId})
	if err != nil {
		log.Println("failed to getting user for post by id: ", err)
		return &p.PostResponse{}, err
	}

	// res.Fullname = user.FirstName + " " + user.LastName
	return res, nil
}

func (s *PostService) DeletePost(ctx context.Context, req *p.PostId) (*p.PostResponse, error) {
	res,err := s.storage.Post().DeletePost(req.Id)
	if err != nil {
		s.Logger.Error("error delete post by user_id", logger.Any("Error delete post by user_id", err))
		return &p.PostResponse{}, status.Error(codes.Internal, "something went wrong, please check post info")
	}

	postUser, err := s.Client.User().UserForCreate(ctx, &u.UserId{Id: res.OwnerId})
	if err != nil {
		log.Println("failed to getting user for delete post: ", err)
		return &p.PostResponse{}, err
	}
	res.Fullname = postUser.FirstName + " " + postUser.LastName
	return res, nil
}
