package main

import (
	"fmt"
	"net"

	"github.com/double/post_service/config"
	p "github.com/double/post_service/genproto/post"
	"github.com/double/post_service/pkg/db"
	"github.com/double/post_service/pkg/logger"
	"github.com/double/post_service/service"
	"github.com/double/post_service/kafka"
	grpcclient "github.com/double/post_service/service/grpc_client"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"github.com/uber/jaeger-client-go"
	jaegercfg "github.com/uber/jaeger-client-go/config"
)

func main() {
	conf := jaegercfg.Configuration{
		Sampler: &jaegercfg.SamplerConfig{
			Type: jaeger.SamplerTypeConst,
			Param: 10,
		},
		Reporter: &jaegercfg.ReporterConfig{
			LogSpans: true,
			LocalAgentHostPort: "jaeger:6831",
		},
	}
	closer, err := conf.InitGlobalTracer(
		"post_service",
	)
	if err != nil{
		fmt.Println(err)
	}
	defer closer.Close()


	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "golang")
	defer logger.Cleanup(log)

	connDb, err := db.ConnectToDb(cfg)
	if err != nil {
		log.Fatal("Error connect postgres", logger.Error(err))
	}

	NewUser := kafka.NewKafkaConsumer(connDb, &cfg, log, "post")
	go NewUser.Start()

	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		fmt.Println("Error connect grpc client: ", err.Error())
	}

	postService := service.NewPostService(connDb, log, grpcClient)
	
	postPort := ":8001"
	lis, err := net.Listen("tcp", postPort)

	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	p.RegisterPostServiceServer(s, postService)

	log.Info("main: server running",
		logger.String("port", cfg.PostServicePort))
	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

}
