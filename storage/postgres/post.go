package postgres

import (
	"context"
	"time"
	"github.com/opentracing/opentracing-go"
	p "github.com/double/post_service/genproto/post"
)

func (r *PostRepo) CreatePost(ctx context.Context, post *p.PostRequest) (*p.PostResponse, error){
	trace, ctx := opentracing.StartSpanFromContext(ctx, "CreatePost")
	defer trace.Finish()
	res := p.PostResponse{}
	err := r.db.QueryRow(`
	INSERT INTO 
		post(title,description,owner_id) 
	VALUES
		($1, $2, $3) 
	RETURNING 
		id, owner_id, title, description, created_at, updated_at`, post.Title, post.Description, post.OwnerId).
		Scan(&res.Id, 
			&res.OwnerId, 
			&res.Title, 
			&res.Description, 
			&res.CreatedAt, 
			&res.UpdatedAt)
	if err != nil {
		return &p.PostResponse{}, err
	}
	return &res, nil
}

func (r *PostRepo) GetPostById(id int64) (*p.PostResponse, error){
	res := p.PostResponse{}
	err := r.db.QueryRow(`
	SELECT 
		id, owner_id, title, description, created_at, updated_at
	FROM 
		post
	WHERE
		id=$1`, id).
		Scan(&res.Id, &res.OwnerId, &res.Title, &res.Description, &res.CreatedAt, &res.UpdatedAt)
	if err != nil {
		return &p.PostResponse{}, err
	}
	return &res, nil
}

func  (r *PostRepo) GetPostByUserId(id int64) (*p.Posts, error){
	var res p.Posts
	rows, err := r.db.Query(`SELECT id, owner_id, title, description, created_at, updated_at FROM post
	WHERE owner_id = $1`, id)
	if err != nil {
		return &p.Posts{}, err
	}
	for rows.Next(){
		temp := p.PostResponse{}
		err = rows.Scan(&temp.Id, &temp.OwnerId, &temp.Title, &temp.Description, &temp.CreatedAt, &temp.UpdatedAt)
		if err != nil{
			return &p.Posts{}, err
		}
		res.Posts = append(res.Posts, &temp)
	}
	return &res, nil
}

func (r *PostRepo) UpdatePost(ctx context.Context, post *p.UpdatePostRequest) (*p.PostResponse, error){
	trace, ctx := opentracing.StartSpanFromContext(ctx, "UpdatePost")
	defer trace.Finish()
	var res p.PostResponse
	err := r.db.QueryRow(`
	UPDATE post SET
    	title = $1, description = $2, updated_at = $3
	WHERE id = $4 AND deleted_at IS NULL
	RETURNING id, owner_id, title, description, created_at, updated_at
	`, post.Title, post.Description, time.Now(), post.Id).Scan(
		&res.Id, 
		&res.OwnerId, 
		&res.Title, 
		&res.Description, 
		&res.CreatedAt, 
		&res.UpdatedAt)
	
		if err != nil {
			return &p.PostResponse{}, err
		}
	return &res, nil
}

func (r *PostRepo) DeletePost(id int64) (*p.PostResponse, error){
	res := p.PostResponse{}
	err := r.db.QueryRow(`
	DELETE FROM post
	WHERE
		id=$1`, id)
	if err != nil {
		return &p.PostResponse{}, nil
	}
	return &res, nil
}