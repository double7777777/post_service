package postgres

import (
	"context"
	"testing"

	"github.com/double/post_service/config"
	pp "github.com/double/post_service/genproto/post"
	"github.com/double/post_service/pkg/db"
	"github.com/double/post_service/storage/repo"
	"github.com/stretchr/testify/suite"
)

type PostSuiteTest struct{
	suite.Suite
	CleanUpFunc func()	
	Repo repo.PostStorageI
}

func (s *PostSuiteTest) SetupSuite() {
	pgPool, cleanUp := db.ConnectToDBForSuite(config.Load())
	s.Repo = NewPostRepo(pgPool)
	s.CleanUpFunc = cleanUp
}

func (s *PostSuiteTest) TestPostCrud() {
	post := &pp.PostRequest{
		Title: "test",
		Description: "comentator",
		OwnerId: 3,
	}

	createPostResp, err := s.Repo.CreatePost(context.Background(),post)
	s.Nil(err)
	s.NotNil(createPostResp)
	s.Equal(post.Title, createPostResp.Title)
	s.Equal(post.Description, createPostResp.Description)
	s.Equal(post.OwnerId, createPostResp.OwnerId)

	getPostResp, err := s.Repo.GetPostById(createPostResp.Id)
	s.Nil(err)
	s.NotNil(getPostResp)
	s.Equal(getPostResp.Title, post.Title)
	s.Equal(getPostResp.Description, post.Description)
	s.Equal(getPostResp.OwnerId, post.OwnerId)

	getUserPostResp, err := s.Repo.GetPostByUserId(createPostResp.OwnerId)
	s.Nil(err)
	s.NotNil(getUserPostResp)

	updateBody := &pp.UpdatePostRequest{
		Title: "muvaffaqiyatli",
		Description: "yangilandi",
		Id: createPostResp.Id,
	}

	UpdatePostResp, err := s.Repo.UpdatePost(context.Background(),updateBody)
	s.Nil(err)
	s.NotNil(UpdatePostResp)
	s.NotEqual(post.Title, UpdatePostResp.Title)
	s.NotEqual(post.Description, UpdatePostResp.Description)
	s.Equal(post.OwnerId, UpdatePostResp.OwnerId)

	DeletePostResp, err := s.Repo.DeletePost(createPostResp.Id)
	s.Nil(err)
	s.NotNil(DeletePostResp)

}

func (suite *PostSuiteTest) TearDownSuite() {
	suite.CleanUpFunc()
}

func TestPostRepositoryTestSuite(t *testing.T) {
	suite.Run(t, new(PostSuiteTest))
}