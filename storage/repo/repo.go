package repo

import (
	"context"

	p "github.com/double/post_service/genproto/post"
)

type PostStorageI interface{
	CreatePost(context.Context, *p.PostRequest) (*p.PostResponse, error)
	GetPostById(id int64) (*p.PostResponse, error)
	GetPostByUserId(id int64) (*p.Posts, error)
	UpdatePost(context.Context, *p.UpdatePostRequest) (*p.PostResponse, error)
	DeletePost(id int64) (*p.PostResponse, error)
}