run:
	go run cmd/main.go

proto-gen:
	./script/gen-proto.sh

migrate_up:
	migrate -path migrations/ -database postgres://yunus:godev@localhost:5432/post_db up

migrate_down:
	migrate -path migrations/ -database postgres://yunus:godev@localhost:5432/post_db down

migrate_force:
	migrate -path migrations/ -database postgres://yunus:godev@localhost:5432/user_db force 1

update_submodule:
	git submodule update --remote --merge

migrate_file:
	migrate create -ext sql -dir migrations -seq create_posts_table



